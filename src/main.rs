use anyhow::anyhow;
use serenity::{async_trait, model::prelude::GuildId};
use serenity::model::channel::Message;
use serenity::model::gateway::Ready;
use serenity::prelude::*;
use shuttle_secrets::SecretStore;
use tracing::{error, info};

struct Bot {
    discord_guild_id: GuildId,
}

#[async_trait]
impl EventHandler for Bot {

    async fn message(&self, ctx: Context, msg: Message) {
        if msg.content == "!hello" {
            if let Err(e) = msg.channel_id.say(&ctx.http, "world!").await {
                error!("Error sending message: {:?}", e);
            }
        }
    }

    async fn ready(&self, ctx: Context, ready: Ready) {
        info!("{} is connected!", ready.user.name);

        let commands = GuildId::set_application_commands(&self.discord_guild_id, &ctx.http, |commands| {
            commands.create_application_command(|command| {
                command.name("jump").description("Do a jump!")
            })
        }).await.unwrap();

        info!("Registered commands: {:#?}", commands);
    }
}

#[shuttle_runtime::main]
async fn serenity(
    #[shuttle_secrets::Secrets] secret_store: SecretStore,
) -> shuttle_serenity::ShuttleSerenity {
    // Get the discord token set in `Secrets.toml`
    let token = if let Some(token) = secret_store.get("DISCORD_TOKEN") {
        token
    } else {
        return Err(anyhow!("'DISCORD_TOKEN' was not found").into());
    };

    // Get the guild ID from Secrets
    let id = if let Some(id) = secret_store.get("DISCORD_GUILD_ID") {
        id
    } else {
        return Err(anyhow!("'DISCORD_GUILD_ID' was not found").into());
    };

    let guild_id = if let Ok(guild_id) = id.parse() {
        guild_id
    } else {
        return Err(anyhow!("'DISCORD_GUILD_ID' was not found").into());
    };

    // Set gateway intents, which decides what events the bot will be notified about
    let intents = GatewayIntents::GUILD_MESSAGES | GatewayIntents::MESSAGE_CONTENT;

    let client = Client::builder(&token, intents)
        .event_handler(Bot {
            discord_guild_id: GuildId(guild_id),
        })
        .await
        .expect("Err creating client");

    Ok(client.into())
}
